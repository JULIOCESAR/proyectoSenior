'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MovimientoSchema = Schema ({
    numEmpleado: Number,
    fecha: Date,
    opcEntrega: Number,
    numEntregas: Number,
    opcTurno: Number,
    refEmpleado: { type: Schema.Types.ObjectId, ref: 'Empleado' },
    refRolTurno: { type: Schema.Types.ObjectId, ref: 'Rol', required: false}
});

module.exports = mongoose.model('Movimiento', MovimientoSchema);
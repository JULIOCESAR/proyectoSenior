'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RolSchema = Schema({
    nomRol: String,
    numBono: String
});

module.exports = mongoose.model('Rol', RolSchema);
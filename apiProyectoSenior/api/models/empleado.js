'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var EmpleadoSchema = Schema ({
    numEmpleado: Number,
    nomEmpleado: String,
    rol: { type: Schema.Types.ObjectId, ref: 'Rol' },
    tipo: { type: Schema.Types.ObjectId, ref: 'Tipo' }
});

module.exports = mongoose.model('Empleado', EmpleadoSchema);
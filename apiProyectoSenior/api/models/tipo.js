'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TipoSchema = Schema({
    nomTipo: String,
});

module.exports = mongoose.model('Tipo', TipoSchema);
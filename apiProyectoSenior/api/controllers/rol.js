'use strict'
var bcrypt = require('bcrypt-nodejs');
var Rol = require('../models/rol');
var jwt = require('../services/jwt');

function guardarRol(req, res) {

    var params = req.body;
    var rol = new Rol();
    if (params.nomRol && params.numBono) {

        rol.nomRol = params.nomRol;
        rol.numBono = params.numBono;

        rol.save((err, rolStored) => {

            if (err) return res.status(500).send({ message: 'Error al guardar el rol' });

            if (rolStored) {

                res.status(200).send({ rol: rolStored });

            } else {

                res.status(400).send({ message: 'No se ha regitrado el rol' })

            }

        });

    } else {

        res.status(200).send({

            message: 'envia todo los campos del rol!'

        });

    }

}

function obtenerRoles(req, res) {

    var rolesCombo = [];
    Rol.find().exec((err, roles) => {
        if (err) return res.status(500).send({ message: err });

        roles.forEach(rol => {
            
            rolesCombo.push({value: rol._id, label: rol.nomRol});

        });

        return res.status(200).send(rolesCombo);
    });

}

function obtenerMovimientosRoles(req, res) {

    var rolesCombo = [];
    Rol.find({ _id: { $ne: '5b4e21e05dd146a8305f2595' }}).exec((err, roles) => {
        if (err) return res.status(500).send({ message: err });

        roles.forEach(rol => {

            rolesCombo.push({ value: rol._id, label: rol.nomRol });

        });

        return res.status(200).send(rolesCombo);
    });

}

module.exports = {
    guardarRol,
    obtenerRoles,
    obtenerMovimientosRoles
};
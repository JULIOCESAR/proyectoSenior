'use strict'
var bcrypt = require('bcrypt-nodejs');
var Movimiento = require('../models/movimiento');
var Empleado = require('../models/empleado');
var jwt = require('../services/jwt');
var async = require('async');

function guardarMovimiento (req, res) {

    var params = req.body;
    var movimiento = new Movimiento();
    if(params.numEmpleado && params.fecha){
        
        movimiento.numEmpleado = params.numEmpleado;
        movimiento.fecha = params.fecha;
        movimiento.opcEntrega = params.opcEntrega ? params.opcEntrega : 0;
        movimiento.numEntregas = params.numEntregas ? params.numEntregas : 0;
        movimiento.opcTurno = params.opcTurno ? params.opcTurno : 0;
        movimiento.refRolTurno = params.refRolTurno ? params.refRolTurno : '5b4e21e05dd146a8305f2595';

        Empleado.findOne({ numEmpleado: params.numEmpleado }, function (err, empl) {

            if (err) return res.status(500).send({ message: err });
            
            movimiento.refEmpleado = empl.id;

            movimiento.save((err, movimientoStored) => {
                            
                if(err) return res.status(500).send({message:'Error al guardar el movimiento'});
                
                if(movimientoStored){
                    
                    res.status(200).send({movimiento: movimientoStored});
    
                }else {
    
                    res.status(400).send({message:'No se ha regitrado el movimiento'})
                
                }
            
            });

        });


    } else {

        res.status(200).send({
            
            message:'envia todo los campos del movimiento necesarios!'
        
        });
    
    }

}

function actualizarMovimiento (req, res) {
    
    var params = req.body;
    var movId = req.params.id;
    params.refRolTurno = params.refRolTurno ? params.refRolTurno : '5b4e21e05dd146a8305f2595';
    if (params.numEmpleado) {

        Movimiento.findByIdAndUpdate(movId, params, { new: true }, (err, movUpdate) => {

            if (err) return res.status(500).send({ message: 'Error al editar el movimiento' });

            if (movUpdate) {

                res.status(200).send({ movimiento: movUpdate });

            } else {

                res.status(400).send({ message: 'No se ha actualizado el movimiento' })

            }

        });

    } else {

        res.status(200).send({

            message: 'envia todo los campos del movimiento necesarios!'

        });

    }

}

function obtenerMovimientos (req, res) {

    var page = 1;
    if(req.params.page){
        var page = req.params.page; 
    }
    var itemsPerPage = 100;
    var nombre = [];

    Movimiento.find().
        populate({ 
            path : 'refEmpleado', 
            populate: { path: 'rol tipo' }
        }).
        exec(function (err, movimientos) {
        if (err) return res.status(500).send({ message: err });
        return res.status(200).send(movimientos);
    });

}

function obtenerMovimiento (req, res) {
    
    var movimientoId = req.params.id;
    Movimiento.findById(movimientoId).
    populate({
        path: 'refEmpleado',
        populate: { path: 'rol tipo' }
    }).exec((err, movimiento) => {

        if (err) return res.status(500).send({ message: 'Error en la peticion' });

        if (!movimiento) return res.status(404).send({ message: 'El movimiento no existe no existe' });

        return res.status(200).send(movimiento);
    
    })

}

function eliminarMovimiento(req, res) {

    var movimientoId = req.params.id;
    if (movimientoId) {

        Movimiento.findByIdAndRemove(movimientoId, { rawResult: true }, (err, movimientoDeleted) => {

            if (err) return res.status(500).send({ message: 'Error al eliminar el movimiento' });

            if (movimientoDeleted) {

                res.status(200).send({ movimiento: movimientoDeleted });

            } else {

                res.status(400).send({ message: 'No se ha eliminar el movimiento.' })

            }

        });

    } else {

        res.status(200).send({

            message: 'se necesita un id para eliminar!'

        });

    }

}

function obtenerNomina (req, res) {

    var fechaNomina = req.body;
    var nomina = [];
    var montoAbono = 0;
    var montoAbonoCubrioTurno = 0;
    var montoAdicional = 0;
    var montoTotalBruto = 0;
    var montoTotal = 0;
    var montoBase = 0;
    var totalNomina = 0;
    var montoDespensa = 0;
    

    Movimiento.find({  fecha: { $gt : fechaNomina.fecha}}).exec((err, results) => {

        Movimiento.aggregate([
            {
                $group: {
                    _id: { numEmpleado: '$numEmpleado', refEmpleado: '$refEmpleado'},  
                    count: {$sum: 1},
                    totalEntregas: { $sum : '$numEntregas'},
                    refEmpleado: { '$first' : '$refEmpleado'},
                    refRolTurno :{ '$first' : '$refRolTurno'},
                    fecha: { '$first' : '$fecha'}
                }
            }
        ]).exec((err, result) => {
    
            Movimiento.populate(result, { 
                path: 'refEmpleado refRolTurno',
                populate: { path : 'rol tipo'}
            }, (err, movimientos) => {
    
                if (err) return res.status(500).send({ message: err });
                movimientos.forEach(mov => {
                    montoBase = (30 * 8) * 30;
                    montoAdicional = Number(mov.totalEntregas) * 5;
                    montoAbono = (Number(mov.refEmpleado.rol.numBono) * 8) * 30;
                    montoAbonoCubrioTurno = (Number(mov.refRolTurno.numBono) * 8) * 30;
                    montoTotalBruto = mov.refEmpleado.tipo._id == "5b4915f1949191b0083e17a5" 
                        ? (montoBase + montoAdicional + montoAbono + montoAbonoCubrioTurno) * 1.04 
                        : montoBase + montoAdicional + montoAbono + montoAbonoCubrioTurno;
                    montoDespensa = mov.refEmpleado.tipo._id == "5b4915f1949191b0083e17a5" ? montoTotalBruto * .04 : 0;
                    montoTotal = montoTotalBruto > 16000 ? montoTotalBruto * .88 : montoTotalBruto * .91;
                    totalNomina = totalNomina + montoTotal;
                    nomina.push({
                        numEmpleado: mov.refEmpleado.numEmpleado,
                        nomEmpleado: mov.refEmpleado.nomEmpleado,
                        numSueldoBase:parseFloat(montoBase).toFixed(2),
                        numBono: parseFloat(montoAbono).toFixed(2),
                        numEntregas: mov.totalEntregas,
                        numTotalBruto: parseFloat(montoTotalBruto).toFixed(2),
                        numISR: montoTotalBruto > 16000
                            ? parseFloat(montoTotalBruto * 0.12).toFixed(2) 
                            : parseFloat(montoTotalBruto * 0.09).toFixed(2),
                        numTotal: parseFloat(montoTotal).toFixed(2),
                        numBonoCubrioTurno: parseFloat(montoAbonoCubrioTurno).toFixed(2),
                        numValeDespensa: parseFloat(montoDespensa).toFixed(2),
                        numBonoAdicional: parseFloat(montoAdicional).toFixed(2)
                    });
                });
    
                if (nomina.length) {
                    nomina[0].totalNomina = parseFloat(totalNomina).toFixed(2);
                }
    
                return res.status(200).send(nomina);
    
            });
            
        });

    });

}

module.exports = {
    guardarMovimiento,
    obtenerMovimientos,
    obtenerMovimiento,
    eliminarMovimiento,
    actualizarMovimiento,
    obtenerNomina
};
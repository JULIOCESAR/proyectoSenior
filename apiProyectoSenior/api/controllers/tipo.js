'use strict'
var bcrypt = require('bcrypt-nodejs');
var Tipo = require('../models/tipo');
var jwt = require('../services/jwt');

function guardarTipo(req, res) {

    var params = req.body;
    var tipo = new Tipo();
    if (params.nomTipo) {

        tipo.nomTipo = params.nomTipo;

        tipo.save((err, tipoStored) => {

            if (err) return res.status(500).send({ message: 'Error al guardar el tipo' });

            if (tipoStored) {

                res.status(200).send({ tipo: tipoStored });

            } else {

                res.status(400).send({ message: 'No se ha regitrado el tipo' })

            }

        });

    } else {

        res.status(200).send({

            message: 'envia todo los campos del rol!'

        });

    }

}

function obtenerTipos(req, res) {

    var tiposCombo = [];
    Tipo.find().exec((err, tipos) => {
        if (err) return res.status(500).send({ message: err });

        tipos.forEach(tipo => {

            tiposCombo.push({ value: tipo._id, label: tipo.nomTipo });

        });

        return res.status(200).send(tiposCombo);
    });

}

module.exports = {
    guardarTipo,
    obtenerTipos
};
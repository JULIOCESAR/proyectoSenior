'use strict'
var bcrypt = require('bcrypt-nodejs');
var Empleado = require('../models/empleado');
var Movimiento = require('../models/movimiento');
var moment = require('moment');
var jwt = require('../services/jwt');

function guardarEmpleado (req, res) {

    var params = req.body;
    var empleado = new Empleado();
    if(params.numEmpleado && params.nomEmpleado && params.rol && params.tipo){
        
        empleado.numEmpleado = params.numEmpleado;
        empleado.nomEmpleado = params.nomEmpleado;
        empleado.rol = params.rol;
        empleado.tipo = params.tipo;

        Empleado.findOne({nomEmpleado: params.nomEmpleado.trim()}, (err, existEmpleado) =>{
            
            if (err) return res.status(500).send({ message: 'Error al buscar empleado.'});
            if (existEmpleado) return res.status(500).send({ message: 'El empleado ya esta registrado.'});

            empleado.save((err, empleadoStored) => {
                            
                if(err) return res.status(500).send({message:'Error al guardar el empleado'});
                
                if(empleadoStored){
                    
                    res.status(200).send({empleado: empleadoStored});
    
                }else {
    
                    res.status(400).send({message:'No se ha regitrado el empleado'})
                
                }
            
            });

        });

    } else {

        res.status(200).send({
            
            message:'envia todo los campos del emplado necesarios!'
        
        });
    
    }

}

function actualizarEmpleado (req, res) {

    var params = req.body;
    var empleadoId = req.params.id;
    if (params.numEmpleado && params.nomEmpleado && params.rol && params.tipo) {

        Empleado.findOne({
            nomEmpleado : params.nomEmpleado.trim(),
            _id : {$ne : empleadoId }
        }).exec((err, existEmpleado) => {

            if (err) return res.status(500).send({message: 'Error al consultar el empleado.'});
            if (existEmpleado) return res.status(500).send({ message: 'El empleado ya esta registrado.' });   

            Empleado.findByIdAndUpdate(empleadoId, params, { new: true }, (err, empleadoUpdate) => {
    
                if (err) return res.status(500).send({ message: 'Error al editar el empleado' });
                if (empleadoUpdate) {
    
                    res.status(200).send({ empleado: empleadoUpdate });
    
                } else {
    
                    res.status(400).send({ message: 'No se ha actualizado el empleado' })
    
                }
    
            });

        })

    } else {

        res.status(200).send({

            message: 'envia todo los campos del emplado necesarios!'

        });

    }

}

function obtenerEmpleados (req, res) {

    var page = 1;
    if(req.params.page){
        var page = req.params.page; 
    }
    var itemsPerPage = 100;
    Empleado.find().sort('_id').populate('rol tipo').paginate(page, itemsPerPage, (err, empleados, total) => {
        
        if(err) return res.status(500).send({message:'Error en la peticion'});

        if(!empleados) return res.status(400).send({message:'No hay empleados'});

        return res.status(200).send({
            empleados : empleados,
            total: total,
            pages : Math.ceil(total/itemsPerPage)
        });
    
    });

}

function obtenerEmpleado (req, res) {
    
    var empleadoId = req.params.id;
    Empleado.findById(empleadoId).populate('rol tipo').exec((err, empleado) => {

        if (err) return res.status(500).send({ message: 'Error en la peticion' });

        if (!empleado) return res.status(404).send({ message: 'El empleado no existe no existe' });

        return res.status(200).send(empleado);
    
    })

}

function eliminarEmpleado(req, res) {

    var empleadoId = req.params.id;

    if (empleadoId) {

        Movimiento.findOne({refEmpleado : empleadoId}).exec((err, movimiento) => {

            if (err) return res.status(500).send({ message: 'Error al consultar el movimiento.' });

            if (movimiento){

                return res.status(500).send({ message: 'El empleado a eliminar tiene movimientos relacionados.' });

            }

            Empleado.findByIdAndRemove(empleadoId, { rawResult: true }, (err, empleadoDeleted) => {
    
                if (err) return res.status(400).send({ message: 'Error al eliminar el empleado' });
    
                if (empleadoDeleted) {
    
                    res.status(200).send({ empleado: empleadoDeleted });
    
                } else {
    
                    res.status(400).send({ message: 'No se ha eliminar el empleado.' })
    
                }
    
            });

        });


    } else {

        res.status(200).send({

            message: 'se necesita un id para eliminar!'

        });

    }

}

function obtenerEmpleadosCombo(req, res) {

    Empleado.find({},'_id nomEmpleado').populate('rol tipo').exec((err, empleados) => {

        if (err) return res.status(500).send({ message: 'Error en la peticion ddd' });

        if (!empleados) return res.status(404).send({ message: 'El empleado no existe no existe' });

        var datos = [];
        empleados.forEach(element => {
          
            datos.push({
                value: element._id, 
                label: element.nomEmpleado
            });

        });
        return res.status(200).send(datos);

    });

}

function obtenerFecha(req, res) {

    return res.status(200).send({ fecha: moment() });

}

module.exports = {
    guardarEmpleado,
    actualizarEmpleado,
    obtenerEmpleados,
    obtenerEmpleado,
    eliminarEmpleado,
    obtenerEmpleadosCombo,
    obtenerFecha
};
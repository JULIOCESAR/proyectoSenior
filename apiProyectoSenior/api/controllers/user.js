'use strict'
var bcrypt = require('bcrypt-nodejs');
var User = require('../models/user');
var jwt = require('../services/jwt');
var moment = require('moment');

function home (req , res){
    var ses = moment().add(30, 'seconds').unix();
    //var ses = 565;
    res.status(200).send({ sessi: ses, actual: moment().unix()});
};

function pruebas (req , res){
    res.status(200).send({
        message:'Accion de pruebas'
    });
};

function saveUser(req, res){
    var params = req.body;
    var user = new User();

    if(params.name && params.surname && params.nick &&
        params.email && params.password){
        
        user.name = params.name;
        user.surname = params.surname;
        user.nick = params.nick;
        user.email = params.email;
        user.role = 'ROLE_USER';
        user.image = null;
        
        // controlar usuarios duplicados
        User.find({ $or: [
            {email: user.email.toLowerCase()},
            {nick: user.nick.toLowerCase()}
        ]}).exec((err, users) => {
            
            if(err) return res.status(500).send({message:'Error en la peticion'});

            if(users && users.length >= 1){
                return res.status(200).send({message: 'Usuario registrado'});
            } else {
                // se guardan el usuario
                bcrypt.hash(params.password, null, null, (err, hash) => {
                    user.password = hash;

                    user.save((err, userStored) => {
                        
                        if(err) return res.status(500).send({message:'Error al guardar el usuario'});
                        
                        if(userStored){
                            res.status(200).send({user: userStored});
                        }else {
                            res.status(400).send({message:'No se ha regitrado el usuario'})
                        }
                    });
                });
            }
        });

    } else {
        res.status(200).send({
            message:'envia todo los campos necesarios!'
        });
    }
}

function loginUser(req, res){
    var params = req.body;

    var email = params.email;
    var password = params.password;

    User.findOne({email : email}, (err, user) => {

        if(err) return res.status(500).send({message:'Error en la peticion'});
        if(user){

            bcrypt.compare(password, user.password, (err, check) => {

                if(check){

                    var token = jwt.createToken(user);
                    return res.status(200).send({user: user, token: token});
                    
                } else {

                   return res.status(400).send({message:'El usuario no se a podido identificar.'}) 
                }

            });

        } else {

            return res.status(400).send({message:'El usuario no se a podido identificar!!.'}) 

        }

    })

}

//Conseguir datos de un usuario

function getUser(req, res){
    var userId = req.params.id;

    User.findById(userId, (err, user) => {
        if(err) return res.status(500).send({message:'Error en la peticion'});

        if(!user) return res.status(404).send({message:'El usuario no existe'});

        return res.status(200).send({user});
    })

}

//Devolver un listado de usuario paginados
function getUsers(req, res){
    var identity_user_id = req.user.sub;
    var page = 1;

    if(req.params.page){
        var page = req.params.page; 
    }

    var itemsPerPage = 5;

    User.find().sort('_id').paginate(page, itemsPerPage, (err, users, total) => {
        if(err) return res.status(500).send({message:'Error en la peticion'});

        if(!users) return res.status(400).send({message:'No hay usuarios'});

        return res.status(200).send({
            users : users,
            total: total,
            pages : Math.ceil(total/itemsPerPage)
        });
    });
}

module.exports = {
    home,
    pruebas,
    saveUser,
    loginUser,
    getUser,
    getUsers
};
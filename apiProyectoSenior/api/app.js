'use strict'

var express = require('express');
var bodyParse = require('body-parser');
var cors = require('cors');

var app = express();

//cargar rutas
var user_routes = require('./routes/user');
var empleados_routes = require('./routes/empleado');
var movimientos_routes = require('./routes/movimiento');
var roles_routes = require('./routes/rol');
var tipos_routes = require('./routes/tipo');

//middlewares
app.use(bodyParse.urlencoded({extended:false}));
app.use(bodyParse.json());
app.use(cors());
//core

// rutas
app.use('/api', user_routes);
app.use('/api', empleados_routes);
app.use('/api', movimientos_routes);
app.use('/api', roles_routes);
app.use('/api', tipos_routes);

//exportar
module.exports = app;

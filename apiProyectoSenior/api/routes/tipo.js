'use strict'

var express = require('express');
var TipoController = require('../controllers/tipo');

var api = express.Router();
var md_auth = require('../middleware/authenticated');

api.get('/tipos', md_auth.ensureAuth, TipoController.obtenerTipos);
api.post('/tipos', md_auth.ensureAuth, TipoController.guardarTipo);
module.exports = api;
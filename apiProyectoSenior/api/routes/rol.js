'use strict'

var express = require('express');
var RolController = require('../controllers/rol');

var api = express.Router();
var md_auth = require('../middleware/authenticated');

api.get('/roles', md_auth.ensureAuth, RolController.obtenerRoles);
api.get('/roles/movimientos', md_auth.ensureAuth, RolController.obtenerMovimientosRoles);
api.post('/roles', md_auth.ensureAuth, RolController.guardarRol);
module.exports = api;
'use strict'

var express = require('express');
var MovimientoController = require('../controllers/movimiento');
var mongoosePaginate = require('mongoose-pagination');

var api = express.Router();
var md_auth = require('../middleware/authenticated');

api.get('/movimientos', md_auth.ensureAuth, MovimientoController.obtenerMovimientos);
api.get('/movimientos/:id', md_auth.ensureAuth, MovimientoController.obtenerMovimiento);
api.post('/movimientos', md_auth.ensureAuth, MovimientoController.guardarMovimiento);
api.put('/movimientos/:id', md_auth.ensureAuth, MovimientoController.actualizarMovimiento);
api.delete('/movimientos/:id', md_auth.ensureAuth, MovimientoController.eliminarMovimiento);
api.post('/nominas/movimientos', md_auth.ensureAuth, MovimientoController.obtenerNomina);

module.exports = api;
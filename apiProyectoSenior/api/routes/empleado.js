'use strict'

var express = require('express');
var EmpleadoController = require('../controllers/empleado');
var mongoosePaginate = require('mongoose-pagination');

var api = express.Router();
var md_auth = require('../middleware/authenticated');

api.get('/empleados', md_auth.ensureAuth, EmpleadoController.obtenerEmpleados);
api.get('/fechas', md_auth.ensureAuth, EmpleadoController.obtenerFecha);
api.get('/empleados/:id', md_auth.ensureAuth, EmpleadoController.obtenerEmpleado);
api.post('/empleados', md_auth.ensureAuth, EmpleadoController.guardarEmpleado);
api.put('/empleados/:id', md_auth.ensureAuth, EmpleadoController.actualizarEmpleado);
api.delete('/empleados/:id', md_auth.ensureAuth, EmpleadoController.eliminarEmpleado);
api.get('/combos/empleados', md_auth.ensureAuth, EmpleadoController.obtenerEmpleadosCombo);

module.exports = api;
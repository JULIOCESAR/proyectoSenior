'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');
var secret = 'proyectoSenior';

exports.createToken = function (user) {
    var paylaod = {
        sub: user._id,
        name: user.name,
        surname : user.surname,
        email: user.email,
        role: user.role,
        image : user.image,
        iat : moment().unix(),
        exp: moment().add(600, 'seconds').unix()
    };
    return jwt.encode(paylaod, secret);
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, Router, RouterModule, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { PaginaValidaLoginComponent  } from "./paginaValidaLogin/pagina-valida-login.component";
import { PaginaCompletaComponent  } from "./paginaCompleta/pagina-completa.component";
import { UsuarioGuardado } from './activate/usuario-guardado';

export const routes: Routes = [
    {
        canActivate: [UsuarioGuardado],
        path: '',
        component: PaginaCompletaComponent,
        children: [
            {
                path: 'empleados',
                loadChildren: './modules/empleados/empleados.module#EmpleadosModule'
            },
            {
                path: 'movimientos',
                loadChildren: './modules/movimientos/movimientos.module#MovimientosModule'
            },
            {
                path: 'nominas',
                loadChildren: './modules/nominas/nominas.module#NominasModule'
            }
        ],
    },
    {
        path: 'login',
        component: PaginaValidaLoginComponent,
        children: [
            {
                path: '',
                loadChildren: './modules/login/login.module#LoginModule',
            },
        ],
    },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, { useHash: true })
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }




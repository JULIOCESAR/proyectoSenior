import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/throw'
import 'rxjs/add/operator/catch';
import { AutenticacionService } from '../services/autenticacion.service';

@Injectable()
export class Interceptor implements HttpInterceptor{

    constructor( public autenticar: AutenticacionService) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const usuarioLogueado = JSON.parse(localStorage.getItem('usuarioLogueado'));
        var authReq = usuarioLogueado ? req.clone(
            {
                headers: req.headers.set("Authorization", usuarioLogueado.token)
            }
        ) : req;

        return next.handle(authReq).catch((error, caught) => {
            if (error.status === 401 || error.status === 401) {
                this.autenticar.limpiarStorage();
            }
            return Observable.throw(error);
        }) as any;
    }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginaValidaLoginComponent } from './pagina-valida-login.component';

describe('PaginaValidaLoginComponent', () => {
  let component: PaginaValidaLoginComponent;
  let fixture: ComponentFixture<PaginaValidaLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaginaValidaLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginaValidaLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { CardModule } from 'primeng/card';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToasterModule, ToasterService } from 'angular5-toaster';
import { Interceptor } from './interceptores/interceptor';
import { PaginaValidaLoginComponent } from './paginaValidaLogin/pagina-valida-login.component';
import { PaginaCompletaComponent } from './paginaCompleta/pagina-completa.component';
import { UsuarioGuardado } from './activate/usuario-guardado';

@NgModule({
    declarations: [
        AppComponent,
        PaginaValidaLoginComponent,
        PaginaCompletaComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        CardModule,
        HttpClientModule,
        CommonModule,
        BrowserAnimationsModule,
        ToasterModule
    ],
    providers: [
        {
        provide: HTTP_INTERCEPTORS,
        useClass: Interceptor,
        multi: true,
        },
        UsuarioGuardado
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }

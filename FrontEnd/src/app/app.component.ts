import { Component, OnInit } from '@angular/core';
import { AutenticacionService } from './services/autenticacion.service'
import { Event, Router, ActivatedRoute, Params, RoutesRecognized } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})

export class AppComponent {
    title = 'app';
    public autorizado: any;
    constructor (
        private route: ActivatedRoute, 
        private router: Router, 
        private autorizar: AutenticacionService
    ) {

    }
    ngOnInit(){
        this.autorizado = this.autorizar.estaAutorizado();
        if (!this.autorizado) {
            this.autorizar.limpiarStorage();
        } 
       
    }

}

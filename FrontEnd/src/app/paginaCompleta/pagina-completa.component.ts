import { Component, OnInit } from '@angular/core';
import { AutenticacionService } from '../services/autenticacion.service'

@Component({
  selector: 'app-pagina-completa',
  templateUrl: './pagina-completa.component.html',
  styleUrls: ['./pagina-completa.component.css']
})
export class PaginaCompletaComponent implements OnInit {

  constructor(private autorizar: AutenticacionService) { }

  ngOnInit() {
  }

  salir () {
    this.autorizar.limpiarStorage();
  }

}

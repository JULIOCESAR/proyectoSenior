import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginaCompletaComponent } from './pagina-completa.component';

describe('PaginaCompletaComponent', () => {
  let component: PaginaCompletaComponent;
  let fixture: ComponentFixture<PaginaCompletaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaginaCompletaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginaCompletaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

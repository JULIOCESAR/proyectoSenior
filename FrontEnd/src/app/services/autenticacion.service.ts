import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute, Params } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AutenticacionService {
    private config = require('../../assets/config.json');
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private http: HttpClient
    ) { }

    estaAutorizado () {
        if (localStorage.getItem('usuarioLogueado')) {
            return true;
        } else {
            return false;
        }
    }

    limpiarStorage () {
        localStorage.removeItem('usuarioLogueado');
        localStorage.removeItem('token');
        this.router.navigate(['/login']);
    }

    login (user: any, password: any) {

        return this.http.post<any>(`${this.config.api}login`, { email: user, password: password }).pipe(map((result) => {

            if (result.token) {
                
                localStorage.setItem('usuarioLogueado', JSON.stringify(result));

            }

            return result;

        })); 
    }

}

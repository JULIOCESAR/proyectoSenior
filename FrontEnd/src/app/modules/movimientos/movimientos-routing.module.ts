import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MovimientosComponent } from "./movimientos.component";
import { MovimientosInformacionComponent } from "./movimientos-informacion.component";


const routes: Routes = [
  { path: '', component: MovimientosComponent },
  { path: 'informacion', component: MovimientosInformacionComponent },
  { path: 'informacion/:id', component: MovimientosInformacionComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MovimientosRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { MovimientosRoutingModule } from './movimientos-routing.module';
import { MovimientosComponent } from './movimientos.component';
import { MovimientosInformacionComponent } from './movimientos-informacion.component';

import { TableModule } from 'primeng/table';
import { ReactiveFormsModule , FormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
//se usan para cargar el lenguaje espanol al datepicker
import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
import { EmpleadosService } from '../empleados/services/empleados.service';
import { MovimientosService } from "../movimientos/services/movimientos.service";
import { RolesService } from '../empleados/services/roles.service';
import { TiposService } from '../empleados/services/tipos.service';
import { NgSelectModule } from '@ng-select/ng-select';
import { BlockUIModule } from 'ng-block-ui';
import { ToasterModule, ToasterService } from 'angular5-toaster';

// se carga el lenguaje espanol
defineLocale('es', esLocale);

@NgModule({
  imports: [
    CommonModule,
    MovimientosRoutingModule,
    TableModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    FormsModule,
    NgSelectModule,
    BlockUIModule.forRoot(),
    ToasterModule
  ],
  declarations: [MovimientosComponent, MovimientosInformacionComponent],
  providers:[
    EmpleadosService,
    MovimientosService,
    DatePipe,
    RolesService,
    TiposService
  ]
})
export class MovimientosModule { }

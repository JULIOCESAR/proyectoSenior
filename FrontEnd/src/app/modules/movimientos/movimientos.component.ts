import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { Router } from "@angular/router";
import Swal from 'sweetalert2'
import { MovimientosService } from "../movimientos/services/movimientos.service";
import { EmpleadosService } from '../empleados/services/empleados.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import {ToasterModule, ToasterService, ToasterConfig} from 'angular5-toaster';

@Component({
  selector: 'app-movimientos',
  templateUrl: './movimientos.component.html',
  styleUrls: ['./movimientos.component.css']
})
export class MovimientosComponent implements OnInit, AfterViewInit {
    
    @BlockUI('listaMovimientos') listMovBlockUI: NgBlockUI;
    @ViewChild('btnAgregar') botonAgregar :ElementRef;
    public movimientos: any;
    public filtrosGlobales: any [];
    public movimientosBd: any;
    public empleadoBd: any;
    
    constructor( 
        private router: Router,
        private movService: MovimientosService,
        private empleadosService: EmpleadosService
    ) { }

    ngOnInit() {
        
        this.filtrosGlobales = [
            'refEmpleado.nomEmpleado', 
            'refEmpleado.rol.nomRol', 
            'refEmpleado.tipo.nomTipo', 
            'fecha', 'numEntregas'
        ];
        this.obtenerMovimientos();
        this.listMovBlockUI.start('Procesando...');

    }

    ngAfterViewInit () {
        this.botonAgregar.nativeElement.focus();
    }

    public agregarMovimiento () {

        this.router.navigate(['movimientos/informacion']);
    
    }

    public editarMovimiento (id: string) {
    
        this.router.navigate(['movimientos/informacion', id]);
    
    }

    public eliminarMovimiento (id: string) {

        Swal({
            title: `Estas seguro de eliminar el movimiento?`,
            text: 'esta acción es irreversible!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Aceptar',
            cancelButtonText:'Cancelar'
        }).then((result) => {

            if (result.value) {
              
                this.movService.eliminarMovimiento(id).subscribe((result) => {

                    this.obtenerMovimientos();

                });

            }

        })

    }

    public obtenerMovimientos () {

        this.movService.obtenerMovimientos().subscribe((movs) => {
    
            this.movimientosBd = movs;
            this.movimientos = this.movimientosBd;
            this.listMovBlockUI.stop();
    
        });
        
    }

}

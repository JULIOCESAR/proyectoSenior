import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class MovimientosService {

  private config = require('../../../../assets/config.json');

  constructor(private http: HttpClient) { }

  public guardarMovimiento(movimiento: any):Observable <any> {
    return this.http.post(`${this.config.api}movimientos`, movimiento);
  }

  public obtenerMovimientos(): Observable<any> {
    return this.http.get(`${this.config.api}movimientos`);
  }

  public obtenerMovimiento(id: string): Observable<any> {
    return this.http.get(`${this.config.api}movimientos/${id}`)
  }

  public actualizarMovimiento(id: string, movimiento: any): Observable<any> {
    return this.http.put(`${this.config.api}movimientos/${id}`, movimiento)
  }

  public eliminarMovimiento(id: string): Observable <any> {
    return this.http.delete(`${this.config.api}movimientos/${id}`)
  }

}

import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { BsDatepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { FormControl, FormBuilder, FormGroup, Validators, Form, FormsModule  } from '@angular/forms';
import { EmpleadosService } from '../empleados/services/empleados.service';
import { NgOption } from '@ng-select/ng-select';
import { MovimientosService } from "../movimientos/services/movimientos.service";
import { RolesService } from '../empleados/services/roles.service';
import { TiposService } from '../empleados/services/tipos.service';
import { DatePipe } from '@angular/common';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ToasterService, ToasterConfig } from 'angular5-toaster';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-movimientos-informacion',
  templateUrl: './movimientos-informacion.component.html',
  styleUrls: ['./movimientos-informacion.component.css']
})
export class MovimientosInformacionComponent implements OnInit {

    @ViewChild('comboEmpleado') comboEmpleado: ElementRef;
    @BlockUI('infoMovBlockUI') infoMovBlockUI: NgBlockUI;
    public locale: string = 'es';
    public movimientosForm : FormGroup;
    public empleadosCombo: any;
    public infoEmpleado: any;
    public infoMovimiento: any;
    public infoMovtoBd: any;
    public tipos: any;
    public roles: any;
    public id: any;
    public fechaServer: any;
    public mesMovimiento:string;
    public mesServer: string;
    public anoMovimiento: string;
    public anoServer: string;
    public toasterconfig : ToasterConfig = 
        new ToasterConfig({
        showCloseButton: true, 
        tapToDismiss: false, 
        timeout: 2000,
        animation: 'fade'
    });

    constructor(
        private route: Router, 
        private localeService: BsLocaleService,
        private build: FormBuilder,
        private empleadosService: EmpleadosService,
        private movService: MovimientosService,
        private datePipe: DatePipe,
        private rolesService: RolesService,
        private tiposService: TiposService,
        private parametros: ActivatedRoute, 
        private toasterService: ToasterService
    ) {
        this.toasterService = toasterService;
     }

    ngOnInit() {

        //se asigna a espanol el datepicker
        this.localeService.use(this.locale);
        this.obtenerEmpleadosCombo();
        this.parametros.params.subscribe((params) => {
            this.crearForm();
            this.id = params['id'];
            if (this.id) {
                
                this.infoMovBlockUI.start('Procesando...');
                this.movService.obtenerMovimiento(this.id).subscribe((result) => {
                    
                    this.infoMovtoBd = result;
                    this.setearInformacionMovimiento(result);
                    
                });
                
            } 

        });
        this.rolesService.obtenerRolesMovimientos().subscribe((roles) => {

            this.roles = roles;

        });

        this.empleadosService.obtenerFechaServer().subscribe((result) => {
            this.fechaServer = result;
        });
        
    }

    ngAfterViewInit() {
    }
    
    public crearForm () {
        
        //se genera la estructura del formulario que manejara la informacion de los movimientos
        this.movimientosForm = this.build.group({

            numEmpleado : new FormControl({ value : null, disabled : true }, [Validators.required]),
            rol: new FormControl({ value : null, disabled: true }, [Validators.required]),
            tipo: new FormControl({ value: null, disabled : true }, [Validators.required]),
            fecha: new  FormControl('', [Validators.required, Validators.maxLength(10)]),
            opcEntrega: new FormControl(false),
            numEntregas: new FormControl({ value: null, disabled: true }, [Validators.maxLength(5)]),
            opcTurno: new FormControl({ value: false, disabled: true }),
            empleadoSeleccionado: new FormControl(null),
            refRolTurno: new FormControl({ value: null, disabled: true })
    
        });

    }

    public regresarListado() {

        this.route.navigate(['/movimientos']);
    
    }

    public obtenerEmpleadosCombo () {

        this.empleadosService.obtenerEmpleadosCombo().subscribe((result) => {

            this.empleadosCombo = result;

        });

    }

    public limpiarCampos () {
        
        this.movimientosForm.patchValue({ numEmpleado: '', nomEmpleado: '', rol: '', tipo: '' });
    
    }

    public limpiarCantidadEntregas() {
    
        if (!this.movimientosForm.get('opcEntrega').value) {

            this.movimientosForm.patchValue({ numEntregas: ''});
            this.movimientosForm.get('numEntregas').disable();

        } else {

            this.movimientosForm.get('numEntregas').enable();

        }

    }

    public limpiarRolCubierto () {
        
        if (!this.movimientosForm.get('opcTurno').value) {

            this.movimientosForm.patchValue({ refRolTurno: null});
            this.movimientosForm.get('refRolTurno').disable();

        } else {

            this.movimientosForm.get('refRolTurno').enable();

        }

    }

    public obtenerEmpleadoSeleccinado () {

        if (this.movimientosForm.get('empleadoSeleccionado').value) {

            this.empleadosService.obtenerEmpleado(this.movimientosForm.get('empleadoSeleccionado').value).
            subscribe((result) => {

                this.infoMovtoBd = result;
                this.setearValoresEmpleado(result);
    
            });

        } else {
            
            this.setearValoresEmpleado(null);   
        
        }
        

    }
    public setearValoresEmpleado (empleado) {
        
        this.movimientosForm.patchValue({ 
            numEmpleado: empleado ? empleado.numEmpleado : '', 
            rol: empleado ? empleado.rol.nomRol : '', 
            tipo: empleado ? empleado.tipo.nomTipo : '' 
        }); 

        if (empleado.rol._id !== '5b4e21e05dd146a8305f2595') {
            
            this.movimientosForm.get('refRolTurno').disable();
            this.movimientosForm.get('opcTurno').disable();
            this.movimientosForm.patchValue({ 
                refRolTurno: null, 
                opcTurno: false
            });

        } else {
            
            this.movimientosForm.get('opcTurno').enable();

        }

    }

    public guardarMovimiento () {

        this.anoMovimiento = this.datePipe.transform(this.movimientosForm.get('fecha').value, 'yyyy');
        this.anoServer = this.datePipe.transform(this.fechaServer.fecha, 'yyyy');
        this.mesMovimiento = this.datePipe.transform(this.movimientosForm.get('fecha').value, 'M');
        this.mesServer = this.datePipe.transform(this.fechaServer.fecha, 'M');

        if (this.anoMovimiento !== this.anoServer || this.mesMovimiento !== this.mesServer) {
            
            this.validarFecha();

        } else {

            this.infoMovimiento = {
                numEmpleado: this.movimientosForm.get('numEmpleado').value,
                fecha: this.datePipe.transform(this.movimientosForm.get('fecha').value,  'yyyy/MM/dd'),
                opcEntrega: this.movimientosForm.get('opcEntrega').value ? 1 : 0,
                numEntregas: this.movimientosForm.get('numEntregas').value ? 
                    this.movimientosForm.get('numEntregas').value : 0,
                opcTurno: this.movimientosForm.get('opcTurno').value ? 1 : 0,
                refRolTurno: this.movimientosForm.get('opcTurno').value ? this.movimientosForm.get('refRolTurno').value : ''
            }; 
            this.infoMovBlockUI.start('Procesando...'); 
            if (!this.id) {
                
                this.movService.guardarMovimiento(this.infoMovimiento).subscribe((resul) => {
    
                    this.toasterService.pop('success', 'OK', 'Movimiento guardado correctamente.');
                    this.movimientosForm.reset();
                    this.infoMovBlockUI.stop();
        
                }, (error) => {
    
                    this.toasterService.pop('error', 'Error.', error.error.message);
                    this.infoMovBlockUI.stop();
    
                });
    
            } else {
    
                this.infoMovimiento.refEmpleado = this.infoMovtoBd.refEmpleado._id;
                this.movService.actualizarMovimiento(this.id, this.infoMovimiento).subscribe((resul) => {
    
                    this.toasterService.pop('success', 'OK', 'Movimiento actualizado correctamente.');
                    this.infoMovBlockUI.stop();
        
                }, (error) => {
    
                    this.toasterService.pop('error', 'Error.', error.error.message);
                    this.infoMovBlockUI.stop();
    
                });
    
            }  

        }

    }

    public setearInformacionMovimiento (movimiento : any) {

        this.movimientosForm.patchValue({
            empleadoSeleccionado: movimiento.refEmpleado._id,
            numEmpleado: movimiento.numEmpleado,
            rol: movimiento.refEmpleado.rol.nomRol,
            tipo: movimiento.refEmpleado.tipo.nomTipo,
            fecha : new Date(movimiento.fecha),
            opcEntrega: movimiento.opcEntrega === 1 ? true : false,
            opcTurno: movimiento.opcTurno === 1 ? true : false,
            numEntregas: movimiento.opcEntrega !== 1 ? null : movimiento.numEntregas,
            refRolTurno: movimiento.refEmpleado.rol._id === '5b4e21e05dd146a8305f2595' ? movimiento.refRolTurno : null
        });  

        if (movimiento.opcEntrega !== 1) {
            
            this.movimientosForm.get('numEntregas').disable();
            
        } else {

            this.movimientosForm.get('numEntregas').enable();
        
        }

        if (movimiento.opcTurno !== 1) {

            this.movimientosForm.get('refRolTurno').disable();
            this.movimientosForm.get('opcTurno').disable();
        
        } else {

            this.movimientosForm.get('refRolTurno').enable();
            this.movimientosForm.get('opcTurno').enable();

        }
        this.infoMovBlockUI.stop(); 

    }

    public validarFecha () {

        Swal({
            title: 'Fecha Movimiento.',
            text: 'El mes y año deben coincidir con la fecha actual.',
            type: 'warning',
            showCancelButton: false,
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {

        })    

    }

}

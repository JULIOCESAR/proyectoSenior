import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovimientosInformacionComponent } from './movimientos-informacion.component';

describe('MovimientosInformacionComponent', () => {
  let component: MovimientosInformacionComponent;
  let fixture: ComponentFixture<MovimientosInformacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovimientosInformacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovimientosInformacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

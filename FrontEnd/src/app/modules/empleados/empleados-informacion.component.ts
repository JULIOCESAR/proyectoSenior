import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute} from "@angular/router";
import { FormControl, FormBuilder, FormGroup, Validators, Form, FormsModule } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EmpleadosService } from './services/empleados.service';
import { RolesService } from './services/roles.service';
import { TiposService } from './services/tipos.service';
import { DatePipe } from '@angular/common';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { ToasterService, ToasterConfig } from 'angular5-toaster';
import { IEmpleado } from './modelos/empleado'

@Component({
  selector: 'app-empleados-informacion',
  templateUrl: './empleados-informacion.component.html',
  styleUrls: ['./empleados-informacion.component.css']
})
export class EmpleadosInformacionComponent implements OnInit, AfterViewInit {

    //se genera la variable para setear el foco
    @ViewChild('empleado') inputEmpleado : ElementRef;
    // variable para manejar el blockeo a un elemento en especifico
    @BlockUI('infoEmpleadoBlock') infoEmpleadoBlock: NgBlockUI;

    // variables
    public tipos : any;
    public roles : any;
    public empleadosForm : FormGroup;
    public empleado: any = {};
    public id: any;
    // configuracion para las alertas toast
    public toasterconfig : ToasterConfig = 
        new ToasterConfig({
        showCloseButton: true, 
        tapToDismiss: false, 
        timeout: 2000,
        animation: 'fade'
    });

    constructor(
        private router :Router, 
        private parametros: ActivatedRoute,
        private build: FormBuilder,
        private http: HttpClient,
        private empleadosService : EmpleadosService,
        private datePipe: DatePipe,
        private rolesService: RolesService,
        private tiposService: TiposService,
        private toasterService: ToasterService
    ) { 
        this.toasterService = toasterService;
    }

    // metodo donde inicia la aplicacion
    ngOnInit() {

        // se genera la estructura del formulario de los empleados
        this.empleadosForm = this.build.group({
            numEmpleado : new FormControl({ value:'', disabled : true }, [Validators.required ]),
            nomEmpleado : new FormControl({ value : '', disabled : false}, [Validators.required, Validators.maxLength(70)]),
            rol: new FormControl ({ value : null, disabled : false }, [Validators.required ]),
            tipo : new FormControl ({ value : null, disabled : false }, [Validators.required])
        });

        this.parametros.params.subscribe((params) => {
            
            this.id = params['id'];
            if (this.id) {
                
                this.empleadosService.obtenerEmpleado(this.id).subscribe((result) =>{

                    this.infoEmpleadoBlock.start('Procesando...');
                    this.setearInformacionEmpleado(result);
                
                });
                
            } else {

                this.asignarNumeroEmpleado();
            
            }

        });

        this.rolesService.obtenerRoles().subscribe((roles) => {

            this.roles = roles;

        });
        this.tiposService.obtenerTipos().subscribe((tipos) => {

            this.tipos = tipos;

        });

    }

    // metodo que se ejecuta una vez cargada la pantalla
    ngAfterViewInit() {
        this.inputEmpleado.nativeElement.focus();
    }
    
    public setearInformacionEmpleado (empleado : any) {
     
        this.empleadosForm.setValue({
            numEmpleado: empleado.numEmpleado,
            nomEmpleado: empleado.nomEmpleado,
            rol: empleado.rol._id,
            tipo: empleado.tipo._id
        });
        this.infoEmpleadoBlock.stop();
        
    }

    public regresarListado () {

        this.router.navigate(['/empleados']);
    
    }

    public asignarNumeroEmpleado () {
        //genera el numero de empleado con respecto a la fecha
        this.empleadosForm.patchValue({ 
            numEmpleado: this.datePipe.transform(new Date(), 'ddMMyhms')
        });
    }

    public guardarEmpleado (empleado: any) {
        
        this.empleado.numEmpleado = this.empleadosForm.get('numEmpleado').value;
        this.empleado.nomEmpleado = this.empleadosForm.get('nomEmpleado').value;
        this.empleado.rol = this.empleadosForm.get('rol').value;
        this.empleado.tipo = this.empleadosForm.get('tipo').value;
        this.infoEmpleadoBlock.start('Procesando...');
        if (!this.id) {

            this.empleadosService.guardarEmplado(this.empleado).subscribe((result) => {

                this.toasterService.pop('success', 'OK', 'Empleado guardado correctamente.');
                this.empleadosForm.reset();
                this.asignarNumeroEmpleado();
                this.ngAfterViewInit();
                this.infoEmpleadoBlock.stop();

            }, (error) => {

                this.toasterService.pop('error', 'Error.', error.error.message);
                this.infoEmpleadoBlock.stop();

            });

        } else {

            this.empleadosService.actualizarEmpleado(this.id, this.empleado).subscribe((result) => {
                
                this.toasterService.pop('success', 'OK', 'Empleado actualizado correctamente.');
                this.infoEmpleadoBlock.stop();
            
            }, (error) => {

                this.toasterService.pop('error', 'Error.', error.error.message);
                this.infoEmpleadoBlock.stop();

            });

        }

    }
    
}

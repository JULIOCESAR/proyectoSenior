import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmpleadosRoutingModule } from './empleados-routing.module';
import { EmpleadosComponent } from './empleados.component';
import { EmpleadosInformacionComponent } from './empleados-informacion.component';


import { TableModule } from 'primeng/table';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { EmpleadosService } from './services/empleados.service';
import { RolesService } from './services/roles.service';
import { TiposService } from './services/tipos.service';
import { DatePipe } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { BlockUIModule } from 'ng-block-ui';
import {ToasterModule, ToasterService} from 'angular5-toaster';


@NgModule({
  imports: [
    CommonModule,
    EmpleadosRoutingModule,
    TableModule,
    ReactiveFormsModule,
    NgSelectModule,
    BlockUIModule.forRoot(),
    ToasterModule
  ],
  declarations: [EmpleadosComponent, EmpleadosInformacionComponent],
  providers:[
    EmpleadosService,
    DatePipe,
    RolesService,
    TiposService
  ]
})
export class EmpleadosModule { }

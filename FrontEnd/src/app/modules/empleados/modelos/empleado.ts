export interface IEmpleado  {
    numEmpleado: number,
    nomEmpleao:string,
    rol: {
        nomRol: string,
        numBono: string,
        _id:string,
    },
    tipo:{
        nomTipo:string,
        _id:string
    },
    _id:string
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpleadosInformacionComponent } from './empleados-informacion.component';

describe('EmpleadosInformacionComponent', () => {
  let component: EmpleadosInformacionComponent;
  let fixture: ComponentFixture<EmpleadosInformacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmpleadosInformacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpleadosInformacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IEmpleado } from '../modelos/empleado'

@Injectable()
export class EmpleadosService {

    private config = require('../../../../assets/config.json');

    constructor(private http: HttpClient) { }

    public guardarEmplado(empleado: IEmpleado): Observable<any> {
        return this.http.post(`${this.config.api}empleados`, empleado);
    }

    public obtenerEmpleados(): Observable<any> {
        return this.http.get(`${this.config.api}empleados`);
    }

    public obtenerEmpleadosCombo(): Observable<any> {
        return this.http.get(`${this.config.api}combos/empleados`);
    }

    public obtenerEmpleado(id: number): Observable<any> {
        return this.http.get(`${this.config.api}empleados/${id}`)
    }

    public actualizarEmpleado(id: number, empleado: IEmpleado): Observable<any> {
        return this.http.put(`${this.config.api}empleados/${id}`, empleado)
    }

    public eliminarEmpleado(id: number): Observable<any> {
        return this.http.delete(`${this.config.api}empleados/${id}`)
    }

    public obtenerFechaServer(): Observable<any> {
        return this.http.get(`${this.config.api}fechas`);
    }

}

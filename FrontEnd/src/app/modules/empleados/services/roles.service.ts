import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class RolesService {

  private config = require('../../../../assets/config.json');

  constructor(private http: HttpClient) { }

  public guardarRol(rol: any): Observable<any> {
    return this.http.post(`${this.config.api}roles`, rol);
  }

  public obtenerRoles(): Observable<any> {
    return this.http.get(`${this.config.api}roles`);
  }

  public obtenerRolesMovimientos(): Observable<any> {  
    return this.http.get(`${this.config.api}roles/movimientos`);
  }

}

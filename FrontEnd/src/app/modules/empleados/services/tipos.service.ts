import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class TiposService {

  private config = require('../../../../assets/config.json');

  constructor(private http: HttpClient) { }

  public guardarTipo(tipo: any): Observable<any> {
    return this.http.post(`${this.config.api}tipos`, tipo);
  }

  public obtenerTipos(): Observable<any> {
    return this.http.get(`${this.config.api}tipos`);
  }

}

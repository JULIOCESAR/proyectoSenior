import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmpleadosComponent } from "./empleados.component";
import { EmpleadosInformacionComponent } from "./empleados-informacion.component";

const routes: Routes = [
  { path: '', component: EmpleadosComponent },
  { path: 'informacion', component: EmpleadosInformacionComponent},
  { path: 'informacion/:id', component: EmpleadosInformacionComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmpleadosRoutingModule { }

import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { Router } from "@angular/router";
import Swal from 'sweetalert2'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EmpleadosService } from './services/empleados.service';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { IEmpleado } from './modelos/empleado'

@Component({
  selector: 'app-empleados',
  templateUrl: './empleados.component.html',
  styleUrls: ['./empleados.component.css']
})
export class EmpleadosComponent implements OnInit, AfterViewInit {

    @ViewChild('btnAgregar') botonAgregar: ElementRef;
    @BlockUI('listaEmpleados') listEmplBlockUI: NgBlockUI;
    public cols: any[];
    public empleados : IEmpleado [];
    public filtrosGlobales : any [];
    public respuesta : any;
    

    constructor(
        private router: Router,
        private http: HttpClient,
        private empleadosService: EmpleadosService 
    ) { 
    
    }
  
    ngOnInit() {

        this.listEmplBlockUI.start('Procesando...');
        this.filtrosGlobales = ['nomEmpleado', 'rol.nomRol', 'tipo.nomTipo'];
        this.obtenerEmpleados();  

    }

    ngAfterViewInit() {
        this.botonAgregar.nativeElement.focus();
    }

    public agregarEmpleado () {
        this.router.navigate(['empleados/informacion']);
    }

    public editarEmpleado (numEmpleado: number) {
        this.router.navigate(['empleados/informacion', numEmpleado]);
    }

    public eliminarEmpleado (id: number, nombre: string) {

        Swal({
            title: `Estas seguro de eliminar al empleado: ${nombre}?`,
            text: 'esta acción es irreversible!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Aceptar',
            cancelButtonText:'Cancelar'
        }).then((result) => {

            if (result.value) {
             
                this.empleadosService.eliminarEmpleado(id).subscribe((result) => {

                    this.obtenerEmpleados();

                }, (error) => {

                    Swal({
                        title: 'No se pudo Eliminar.',
                        text: error.error.message,
                        type: 'warning',
                        showCancelButton: false,
                        confirmButtonText: 'Aceptar',
                        cancelButtonText: 'Cancelar'
                    }).then((result) => {

                    })    

                });

            }

        })

    }

    public obtenerEmpleados() {
        
        this.empleadosService.obtenerEmpleados().subscribe((result) => {
            this.respuesta = result;
            this.empleados = this.respuesta.empleados;
            this.listEmplBlockUI.stop();
        });

    }

}

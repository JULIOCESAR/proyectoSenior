import { Component, OnInit } from '@angular/core';
import { NominaService } from './services/nomina.service'
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { BsDatepickerConfig, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-nominas',
  templateUrl: './nominas.component.html',
  styleUrls: ['./nominas.component.css']
})
export class NominasComponent implements OnInit {

    @BlockUI('listaNomina') listaNomBlockUI : NgBlockUI;
    public nomina: any;
    public filtrosGlobales: any [];
    public totalNomina: any;
    public locale: string = 'es';
    public fechaNomina: any;
    public mes: string;
    public ano: string;

    constructor(
        private nominaService : NominaService,
        private localeService: BsLocaleService,
        private datePipe: DatePipe
    ) { }

    ngOnInit() {
        //se asigna a espanol el datepicker
        this.localeService.use(this.locale);
        this.filtrosGlobales = [
            'nomEmpleado'
        ];
        
        
    }

    public buscarNomina() {
        this.listaNomBlockUI.start('Procesando...');
        this.nominaService.obtenerNomina(this.fechaNomina).subscribe((result) => {

            this.nomina = result;
            this.totalNomina = this.nomina.length ? this.nomina[0].totalNomina : ''
            this.listaNomBlockUI.stop();
            this.listaNomBlockUI.stop();
        });

    }

}

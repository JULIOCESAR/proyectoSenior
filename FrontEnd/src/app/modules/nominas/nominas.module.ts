import { NgModule } from '@angular/core';
import { CommonModule, DatePipe} from '@angular/common';

import { NominasRoutingModule } from './nominas-routing.module';
import { NominasComponent } from './nominas.component';
import { NominaService } from './services/nomina.service';
import { TableModule } from 'primeng/table';
import { NgSelectModule } from '@ng-select/ng-select';
import { ReactiveFormsModule , FormsModule } from '@angular/forms';
import { BlockUIModule } from 'ng-block-ui';
import { ToasterModule, ToasterService } from 'angular5-toaster';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
//se usan para cargar el lenguaje espanol al datepicker
import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
// se carga el lenguaje espanol
defineLocale('es', esLocale);

@NgModule({
  imports: [
    CommonModule,
    NominasRoutingModule,
    TableModule,
    NgSelectModule,
    ReactiveFormsModule,
    FormsModule,
    BlockUIModule.forRoot(),
    ToasterModule,
    BsDatepickerModule.forRoot()
  ],
  declarations: [NominasComponent],
  providers:[NominaService, DatePipe]
})
export class NominasModule { }

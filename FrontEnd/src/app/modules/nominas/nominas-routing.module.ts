import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NominasComponent } from "./nominas.component";

const routes: Routes = [
     { path :'', component: NominasComponent} 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NominasRoutingModule { }

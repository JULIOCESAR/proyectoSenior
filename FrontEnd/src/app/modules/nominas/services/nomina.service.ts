import { Injectable } from '@angular/core';
import  { HttpClient, HttpHeaders} from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable()
export class NominaService {

    private config = require('../../../../assets/config.json');

    constructor(private http: HttpClient) { }

    public obtenerNomina(fecha: Date): Observable<any> {

        return this.http.post(`${this.config.api}nominas/movimientos`, {fecha: fecha});  
    
    }


}

import { Component, OnInit, ViewChild, AfterViewInit,ElementRef } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators, Form, FormsModule } from '@angular/forms';
import { AutenticacionService } from "../../services/autenticacion.service";
import { Event, Router, ActivatedRoute, Params, RoutesRecognized } from '@angular/router';
import { ToasterService, ToasterConfig } from 'angular5-toaster';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, AfterViewInit {
    @ViewChild('email') inputEmail: ElementRef;
    public loginForm: FormGroup;
    public user: any;
    public password: any;
    public url: string;
    public toasterconfig : ToasterConfig = 
        new ToasterConfig({
        showCloseButton: true, 
        tapToDismiss: false, 
        timeout: 2000,
        animation: 'fade'
    });
    constructor(
        private build: FormBuilder,
        private autenticar: AutenticacionService,
        private route: ActivatedRoute,
        private router: Router,
        private toasterService: ToasterService
    ) {
        this.toasterService = toasterService;
     }

    ngOnInit() {

        this.url = this.route.snapshot.queryParams['returnUrl'] || '/';
        this.loginForm = this.build.group({
        email : new FormControl('', Validators.required),
        password : new FormControl('', Validators.required)
        });

    }

    ngAfterViewInit () {
        this.inputEmail.nativeElement.focus();
    }

    login () {

        this.user = this.loginForm.get('email').value;
        this.password = this.loginForm.get('password').value;
        this.autenticar.login(this.user, this.password).subscribe((user) =>{
            console.log(user);
            this.router.navigate([this.url]);
        }, (error) => {
            
            this.toasterService.pop('error', 'Error.', error.error.message);

        });

    }


}

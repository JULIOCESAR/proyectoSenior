import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BlockUIModule } from 'ng-block-ui';
import { ToasterModule, ToasterService } from 'angular5-toaster';

@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule,
    ReactiveFormsModule,
    BlockUIModule.forRoot(),
    ToasterModule
  ],
  declarations: [LoginComponent]
})
export class LoginModule { }
